﻿using AutoIt;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FaceBookSeleniumCustomApp
{
    class Program
    {
        static Config _config;
        static List<Post> _posts = new List<Post>();
        static string FaceBookUrl = "https://www.facebook.com/";
        private static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.WriteExceptionToConsole(ex, DateTime.Now);
            }
            return returnObject;
        }
        public static void SaveImage(string imageUrl, string fileName, ImageFormat format)
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(imageUrl);
            Bitmap bitmap; bitmap = new Bitmap(stream);

            if (bitmap != null)
            {
                bitmap.Save(fileName, format);
            }
            stream.Flush();
            stream.Close();
            client.Dispose();
        }
        static void PullData()
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(_config.MiscConfig.ApiUrl);
                _posts = JsonConvert.DeserializeObject<List<Post>>(json);
                foreach (Post post in _posts)
                {
                    post.Images = new List<Image>();
                    foreach (string imageUrl in post.images)
                    {
                        string fileName = AppDomain.CurrentDomain.BaseDirectory + "Images\\" + Path.GetFileName(imageUrl);
                        SaveImage(imageUrl, fileName, ImageFormat.Jpeg);
                        post.Images.Add(new Image
                        {
                            Path = fileName,
                            Url = imageUrl
                        });
                    }
                }
            }
        }
        static void DeleteAllFilesInDirectory(string path)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(path);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch (Exception e)
            { }
        }
        static void InitDirectory()
        {
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Images"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Images");
            }
            else
            {
                if (_config.MiscConfig.ImageFlush)
                    DeleteAllFilesInDirectory(AppDomain.CurrentDomain.BaseDirectory + "Images");

            }
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Session"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Session");
            }
        }
        static void RunAutomation(List<Post> posts)
        {
            IWebDriver driver = null;
            try
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--log-level=3");
                options.AddArguments("--disable-extensions"); // to disable extension
                options.AddArguments("--disable-notifications"); // to disable notification
                options.AddArguments("user-data-dir=" + AppDomain.CurrentDomain.BaseDirectory + "Session");
                driver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, options);
                driver.Navigate().GoToUrl("https://www.facebook.com");
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                IWebElement element = null/* TODO Change to default(_) if this is not a reference type */;
                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@data-click='home_icon']")));
                }
                catch (Exception e)
                {
                    //If not found that means not logged in, so login
                    element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("email")));
                    element.SendKeys(_config.Login.UserName);
                    element = driver.FindElement(By.Id("pass"));
                    element.SendKeys(_config.Login.PassWord);
                    driver.FindElement(By.XPath("//input[@type='submit']")).Click();
                }
                if (!_config.MiscConfig.IsProd)
                {
                    Post posti = _posts.First();
                    posti.Id = "107989473889476";
                    posti.Text = "Test Post";
                    GotoPageAndUploadImage(driver, element, wait, posti);
                }
                else
                {
                    foreach (Post post in posts)
                    {
                        GotoPageAndUploadImage(driver, element, wait, post);
                    }
                }
                RunScroll(driver, element, wait);
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            try
            {
                if (driver != null)
                {
                    driver.Close();
                    driver.Quit();
                }
            }
            catch (Exception ex)
            {

            }
        }
        static void Main(string[] args)
        {
            _config = DeserializeXMLFileToObject<Config>(AppDomain.CurrentDomain.BaseDirectory + "AppConfig.xml");
            InitDirectory();
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMinutes(10);
            Timer timer = new Timer((e) =>
            {
                PullData();
            }, null, startTimeSpan, periodTimeSpan);
            while (1 == 1)
            {
                List<Post> posts = new List<Post>();
                DateTime now = DateTime.Now;
                if (_config.MiscConfig.IsProd)
                {
                    if (_posts.Any(time => now > time.Time && now < time.Time.Add(new TimeSpan(0, 5, 0))))
                    {
                        posts = _posts.Where(time => now > time.Time && now < time.Time.Add(new TimeSpan(0, 5, 0))).ToList();
                        RunAutomation(posts);
                    }
                }
                else
                {
                    RunAutomation(_posts);
                }
                Thread.Sleep(10000);
            }
        }        
        private static void GotoPageAndUploadImage(IWebDriver driver, IWebElement element, WebDriverWait wait, Post post)
        {
            driver.Navigate().GoToUrl(FaceBookUrl + post.Id);
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@data-testid='status-attachment-mentions-input']")));
            element.Click();
            element.SendKeys(post.Text);
            Thread.Sleep(2000);
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[contains(text(),'Photo/Video')]")));
            element.Click();
            try

            {
                Thread.Sleep(5000);
                element = driver.FindElements(By.XPath("//input[@type='file']")).First();
                element = element.FindElement(By.XPath("./.."));
                element.Click();
                AutoItX.WinWait("Open", "", 10000);
                if (AutoItX.WinExists("Open", "") == 1)
                {
                    AutoItX.ControlFocus("Open", "", "Edit1");
                    AutoItX.Sleep(2000);
                    AutoItX.ControlSetText("Open", "", "Edit1", String.Join(" ", post.Images.Select(x => "\"" + x.Path + "\"")));
                    AutoItX.Sleep(2000);
                    AutoItX.Send("{Enter}");
                }
            }
            catch (Exception e)
            { }
            Thread.Sleep(5000);
            if (_config.MiscConfig.IsProd)
            {
                element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[@data-testid='react-composer-post-button']")));
                element.Click();
            }
            Thread.Sleep(6000);
        }
        private static void RunScroll(IWebDriver driver, IWebElement element, WebDriverWait wait)
        {
            driver.Navigate().GoToUrl(FaceBookUrl);
            if (!_config.MiscConfig.IsProd)
            {
                try
                {
                    driver.SwitchTo().Alert().Accept();
                }
                catch (NoAlertPresentException Ex)
                { }
            }
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@data-click='home_icon']")));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            int i = 0;
            Stopwatch s = new Stopwatch();
            Random r = new Random();
            int rndDuration = r.Next(10, 120);
            int rndSpeed = r.Next(1, 3);
            s.Start();
            while (s.Elapsed < TimeSpan.FromSeconds(rndDuration))
            {
                i = i + rndSpeed;
                js.ExecuteScript("window.scrollTo(0, " + (i) + ");");
            }
            s.Stop();
        }
    }
}
