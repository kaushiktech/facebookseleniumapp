﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceBookSeleniumCustomApp
{
    public class Post
    {
        public DateTime Time
        {
            get
            {
                return DateTime.Parse(PublishTimeUTC).ToLocalTime();
            }
        }
        
        [JsonProperty("facebook_id")]
        public string Id { get; set; }
        [JsonProperty("images")]
        public List<String> images { get; set; }
        public List<Image> Images { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("publish_time")]
        public string PublishTimeUTC { get; set; }
    }
    public class Image
    {

        public string Url { get; set; }
        public string Path { get; set; }
    }
}
