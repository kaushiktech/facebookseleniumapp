﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FaceBookSeleniumCustomApp
{
    public class Config
    {
        public Login Login { get; set; }
        public MiscConfig MiscConfig{ get; set; }
        [XmlArray("Posts")]
        [XmlArrayItem("Post")]
        public List<Post> Posts { get; set; }
    }    
    public class Login
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
    public class MiscConfig
    {
        public string ApiUrl { get; set; }        
        public bool IsProd { get; set; }        
        public bool ImageFlush { get; set; }
    }
}
