﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FacebookSeleniumDeleteApp
{
    class Program
    {
        static Config _config;
        static string FaceBookUrl = "https://www.facebook.com/";
        static void InitDirectory()
        {
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Session"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Session");
            }
        }
        private static void RunDeleteAutomation(IWebDriver driver, IWebElement element, WebDriverWait wait, string deleteUrl)
        {
            driver.Navigate().GoToUrl(deleteUrl);
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[contains(@class, 'fbPhotoSnowliftContainer')]")));
            while (1 == 1)
            {
                try
                {
                    element = driver.FindElement(By.XPath("//div[contains(@class, 'fbPhotoSnowliftContainer')]"));
                    element.FindElement(By.XPath("//a[@data-action-type='open_options_flyout']")).Click();
                    Thread.Sleep(1000);
                    element.FindElements(By.XPath("//a[@data-action-type='delete_photo']")).Last().Click();                    
                    if (_config.MiscConfig.IsDeleteEnabled)
                    {
                        element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class,'layerConfirm')]")));
                        element.Click();
                    }

                    else
                    {
                        element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(@class,'layerCancel')]")));
                        element.Click();
                    }
                    Thread.Sleep(3000);
                    element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//a[contains(@class,'snowliftPager next')]")));
                    element.Click();
                }
                catch (Exception e)
                {
                    throw e;
                }
                //if (flag)
                //    exitCondition = true;
                //element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("fbPhotoSnowliftPositionAndCount")));
                //pageOf = element.Text.Trim().Split(new string[] { "of" }, StringSplitOptions.None);
                //flag = (pageOf[0].ToString().Trim() == pageOf[1].ToString().Trim());
            }
        }
        static void Main(string[] args)
        {
            _config = DeserializeXMLFileToObject<Config>(AppDomain.CurrentDomain.BaseDirectory + "AppConfig.xml");
            InitDirectory();
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromMinutes(10);
            RunAutomation();
        }
        private static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.WriteExceptionToConsole(ex, DateTime.Now);
            }
            return returnObject;
        }
        static void RunAutomation()
        {
            IWebDriver driver = null;
            try
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--log-level=3");
                options.AddArguments("--disable-extensions"); // to disable extension
                options.AddArguments("--disable-notifications"); // to disable notification
                options.AddArguments("user-data-dir=" + AppDomain.CurrentDomain.BaseDirectory + "Session");
                driver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, options);
                driver.Navigate().GoToUrl("https://www.facebook.com");
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                IWebElement element = null/* TODO Change to default(_) if this is not a reference type */;
                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@data-click='home_icon']")));
                }
                catch (Exception e)
                {
                    //If not found that means not logged in, so login
                    element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("email")));
                    element.SendKeys(_config.Login.UserName);
                    element = driver.FindElement(By.Id("pass"));
                    element.SendKeys(_config.Login.PassWord);
                    driver.FindElement(By.XPath("//input[@type='submit']")).Click();
                }
                if (!String.IsNullOrEmpty(_config.MiscConfig.DeleteUrl))
                {
                    RunDeleteAutomation(driver, element, wait, _config.MiscConfig.DeleteUrl);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            try
            {
                if (driver != null)
                {
                    driver.Close();
                    driver.Quit();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
