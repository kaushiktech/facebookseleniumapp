﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FacebookSeleniumDeleteApp
{
    public class Config
    {
        public Login Login { get; set; }
        public MiscConfig MiscConfig{ get; set; }        
    }    
    public class Login
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
    public class MiscConfig
    {     
        public bool IsDeleteEnabled { get; set; }
     
        public string DeleteUrl { get; set; }
     
    }
}
